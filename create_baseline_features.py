from collections import namedtuple
import csv
import glob
import os
import sys

def get_utterances_from_file(dialog_csv_file):
    """Returns a list of DialogUtterances from an open file."""
    reader = csv.DictReader(dialog_csv_file)
    return [_dict_to_dialog_utterance(du_dict) for du_dict in reader]

def get_utterances_from_filename(dialog_csv_filename):
    """Returns a list of DialogUtterances from an unopened filename."""
    with open(dialog_csv_filename, "r") as dialog_csv_file:
        return get_utterances_from_file(dialog_csv_file)

DialogUtterance = namedtuple(
    "DialogUtterance", ("act_tag", "speaker", "pos", "text"))

PosTag = namedtuple("PosTag", ("token", "pos"))


def _dict_to_dialog_utterance(du_dict):
    """Private method for converting a dict to a DialogUtterance."""

    # Remove anything with 
    for k, v in du_dict.items():
        if len(v.strip()) == 0:
            du_dict[k] = None

    # Extract tokens and POS tags
    if du_dict["pos"]:
        du_dict["pos"] = [
            PosTag(*token_pos_pair.split("/"))
            for token_pos_pair in du_dict["pos"].split()]
    return DialogUtterance(**du_dict)


op = ""
prev = ""  
sentence = ""
fileUtterance = get_utterances_from_filename(sys.argv[1])
action = ""
speaker = ""
for items in fileUtterance:
    action = items.act_tag
    dialogs_token = ""
    dialogs_pos = ""
    if isinstance(items.pos,list):
        for item in items.pos:
            dialogs_token += "TOKEN_"+item.token+"\t"
            dialogs_pos += "POS_"+item.pos+"\t"
    if prev == "":
        sentence = action+"\tSTART\t"+dialogs_token+"\t"+dialogs_pos+"\n"
        prev = items.speaker
    elif items.speaker == prev:
        sentence = action+"\t"+dialogs_token+"\t"+dialogs_pos+"\n"
    elif items.speaker != prev:
        sentence = action+"\tCHANGED\t"+dialogs_token+"\t"+dialogs_pos+"\n"
        prev = items.speaker
    op += sentence
op += "\n"
print(str(op),end="")