import glob
import subprocess
import sys

files = glob.glob("../csci544_hw3_data/data/train/*")
files = sorted(files)
for f in files:
	op = ""
	cmd = ["python3","create_baseline_features.py",f]
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, cwd = "." )
	out, err = p.communicate()
	print(out.decode('utf-8'),end="")

