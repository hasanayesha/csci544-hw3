import random
import math

print ("Started")
#with open("baseline/baseline_features_final.train") as ipFile:
with open("advanced/withText.train") as ipFile:
	lines = ipFile.readlines()
	temp = []
	tempstr = ""
	for line in lines:
		if line != "\n":
			tempstr += line
		else:
			temp.append(tempstr)
			tempstr = ""

trainingData = int(math.ceil(0.75*len(temp)))
randomNos = random.sample(range(0, len(temp)), trainingData)
notChosen = set(range(0,len(temp))) - set(randomNos)
notChosen = list(notChosen)

traindata = open("Split/","w")

for indexes in randomNos:
	traindata.write(temp[indexes]+"\n")

traindata.close()

testdata = open("Split/","w")

for inds in notChosen:
	testdata.write(temp[inds]+"\n")

testdata.close()
print ("Finished")