from collections import namedtuple
import csv
import glob
import os
import sys

def increaseAccuracy(items):
	lenitList = len(items)
	c = 0
	token = ""
	pos = ""
	for ind in range(lenitList):
		token += "w["+str(c)+"]="+items[ind].token.lower()+"\t"
		pos += "pos["+str(c)+"]="+items[ind].pos+"\t"
		c += 1
	c = 0
	for ind1 in range(0,lenitList-1):
		token += "w["+str(c)+"]|w["+str(c+1)+"]="+items[ind1].token.lower()+"|"+items[ind1+1].token.lower()+"\t"
		pos += "pos["+str(c)+"]|pos["+str(c+1)+"]="+items[ind1].pos+"|"+items[ind1+1].pos+"\t"	
		c += 1
	c = 0
	# for ind2 in range(0,lenitList-2):
	# 	token += "w["+str(c)+"]|w["+str(c+1)+"]|w["+str(c+2)+"]="+items[ind2].token+"|"+items[ind2+1].token+"|"+items[ind2+2].token+"\t"
	# 	pos += "pos["+str(c)+"]|pos["+str(c+1)+"]|pos["+str(c+2)+"]="+items[ind2].pos+"|"+items[ind2+1].pos+"|"+items[ind2+2].pos+"\t"		
	# 	c += 1
	return [token, pos] 



def getConvertedTuple(items,count):
	prev = ""
	sentence = ""
	action = items.act_tag
	dialogs_token = ""
	dialogs_pos = ""
	text1 = ""
	text1 = items.text.replace(" ","\t")
	text1 = text1.lower()
	if isinstance(items.pos,list):
		dialogs_token, dialogs_pos = increaseAccuracy(items.pos)
	if count == 0:
	    sentence = action+"\tSTART\t"+dialogs_token+"\t"+dialogs_pos+"\t"+text1+"\n"
	    prev = items.speaker
	elif prev == items.speaker:
	    sentence = action+"\t"+dialogs_token+"\t"+dialogs_pos+"\t"+text1+"\n"
	elif prev != items.speaker:
	    sentence = action+"\tCHANGED\t"+dialogs_token+"\t"+dialogs_pos+"\t"+text1+"\n"
	    prev = items.speaker
	return sentence

def get_utterances_from_file(dialog_csv_file):
    """Returns a list of DialogUtterances from an open file."""
    reader = csv.DictReader(dialog_csv_file)
    return [_dict_to_dialog_utterance(du_dict) for du_dict in reader]

def get_utterances_from_filename(dialog_csv_filename):
    """Returns a list of DialogUtterances from an unopened filename."""
    with open(dialog_csv_filename, "r") as dialog_csv_file:
        return get_utterances_from_file(dialog_csv_file)

DialogUtterance = namedtuple(
    "DialogUtterance", ("act_tag", "speaker", "pos", "text"))

PosTag = namedtuple("PosTag", ("token", "pos"))


def _dict_to_dialog_utterance(du_dict):
    """Private method for converting a dict to a DialogUtterance."""

    # Remove anything with 
    for k, v in du_dict.items():
        if len(v.strip()) == 0:
            du_dict[k] = None

    # Extract tokens and POS tags
    if du_dict["pos"]:
        du_dict["pos"] = [
            PosTag(*token_pos_pair.split("/"))
            for token_pos_pair in du_dict["pos"].split()]
    return DialogUtterance(**du_dict)

op = "" 
sentence = ""
fileUtterance = get_utterances_from_filename(sys.argv[1])
action = ""
speaker = ""
totalLines= len(fileUtterance)-1
count=0
while count <= totalLines:
	sentence = getConvertedTuple(fileUtterance[count],count)
	count += 1
	op += sentence
op += "\n"
print(str(op),end="")
